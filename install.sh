#!/bin/bash

path="${PWD#"$HOME"/}"

filenames=(
  ".editorconfig"
  ".gemrc"
  ".gitconfig"
  ".irbrc"
  ".tmux.conf"
  ".tmux.conf.local"
  ".zprofile"
)

# Iterate over each repository name
for filename in "${filenames[@]}"; do
  if [ -f $filename ]; then
    [ -f $HOME/$filename ] && mv $HOME/$filename $HOME/$filename.old > /dev/null
  fi

  ln -sf $path/$filename $HOME/$filename
done

# if [ -f vimrc ]; then
# 	[ -f ~/.vimrc ] && mv ~/.vimrc ~/.vimrc.old > /dev/null

#   ln -sf $HOME/.vim/vimrc $HOME/.vimrc
# fi

# if [ -f gvimrc ]; then
#   [ -f ~/.gvimrc] && mv ~/.gvimrc ~/.gvimrc.old > /dev/null
#   ln -sf gvimrc ~/.gvimrc
# fi
