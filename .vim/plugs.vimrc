call plug#begin('~/.vim/plugged')

"Plug 'tpope/vim-sensible'
Plug 'tpope/vim-vinegar'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-repeat'
Plug 'tpope/vim-fugitive'
Plug 'sheerun/vim-polyglot'
Plug 'https://github.com/vim-scripts/csv.vim.git'
Plug 'junegunn/vim-easy-align'
" Start interactive EasyAlign in visual mode (e.g. vipga)
xmap ga <Plug>(EasyAlign)
" Start interactive EasyAlign for a motion/text object (e.g. gaip)
nmap ga <Plug>(EasyAlign)

Plug 'ctrlpvim/ctrlp.vim'
set wildignore+=*/tmp/*,*.so,*.swp,*.swo,*.zip,*/vendor/*
let g:ctrlp_map = '<c-p>'
let g:ctrlp_match_window = 'min:4,max:20'
"let g:ctrlp_match_window = 'results:50'

" Use The Silver Searcher https://github.com/ggreer/the_silver_searcher
if executable('ag')
  " Use Ag over Grep
  set grepprg=ag\ --nogroup\ --nocolor

  " Use ag in CtrlP for listing files. Lightning fast and respects .gitignore
  let g:ctrlp_user_command = 'ag %s -l --nocolor -g ""'

  " ag is fast enough that CtrlP doesn't need to cache
  let g:ctrlp_use_caching = 0
endif

"let g:ctrlp_cmd = 'CtrlP'
"Plug 'tacahiroy/ctrlp-funky'
" Map space in normal mode to CtrlP
"nnoremap <c-p> :CtrlP <CR>
"nnoremap <c-p><c-p> :CtrlPFunky <CR>

"nnoremap <Leader>fu :CtrlPFunky<Cr>
" narrow the list down with a word under cursor
"nnoremap <Leader>fU :execute 'CtrlPFunky ' . expand('<cword>')<Cr>
"let g:ctrlp_funky_matchtype = 'path'
"let g:ctrlp_funky_syntax_highlight = 1

Plug 'editorconfig/editorconfig-vim'
let g:EditorConfig_exclude_patterns = ['fugitive://.*', 'scp://.*']

Plug 'scrooloose/nerdtree'
let NERDTreeIgnore=['\.pyc$', '\~$']
map <Leader>n :NERDTreeToggle<CR>

Plug 'airblade/vim-gitgutter'
"set updatetime=250

Plug 'stanangeloff/php.vim'

Plug 'slashmili/alchemist.vim'
let g:alchemist_tag_disable = 1

Plug 'rorymckinley/vim-rubyhash'

Plug 'vim-ruby/vim-ruby'
Plug 'pangloss/vim-javascript'
Plug 'tpope/vim-markdown'
Plug 'elzr/vim-json'

Plug 'tpope/vim-git'
Plug 'tpope/vim-endwise'
Plug 'godlygeek/tabular'
Plug 'tpope/vim-dispatch'
Plug 'mileszs/ack.vim'
" if executable(ag)
"   let g:ackprg = 'ag --vimgrep --nogroup --nocolor --column'
" endif
let g:ackprg = 'ack --ignore-dir={vendor}'
map <leader>a :Ack<space>

Plug 'AndrewRadev/switch.vim'
Plug 'terryma/vim-multiple-cursors'
Plug 'itchyny/lightline.vim'
set laststatus=2
set noshowmode

" Plug 'mattn/emmet-vim'
Plug 'udalov/kotlin-vim'
Plug 'chriskempson/base16-vim'

call plug#end()

autocmd VimEnter *
  \ if !empty(filter(copy(g:plugs), '!isdirectory(v:val.dir)'))
  \|  PlugInstall | q
  \|endif
