bindkey -e

export LC_CTYPE=en_US.UTF-8
export LANG=en_US.UTF-8

# Check if zplug is installed
if [[ ! -d ~/.zplug ]]; then
  git clone https://github.com/zplug/zplug ~/.zplug
  source ~/.zplug/init.zsh && zplug update --self
fi
#
source ~/.zplug/init.zsh

# Let zplug manage itself
zplug 'zplug/zplug', hook-build:'zplug --self-manage'

#zplug "hkupty/ssh-agent"

zplug "plugins/git", from:oh-my-zsh
zplug "Seinh/git-prune"
zplug "petdance/ack2", as:command
zplug "supercrabtree/k"

#zplug "jhawthorn/fzy", \
#    as:command, \
#    rename-to:fzy, \
#    hook-build:"make && sudo make install"

#zplug "b4b4r07/enhancd", use:init.sh
#if zplug check "b4b4r07/enhancd"; then
    #export ENHANCD_FILTER="fzf --height 50% --reverse --ansi --preview 'ls -l {}' --preview-window down"
#    export ENHANCD_FILTER="fzf --height 50% --reverse --ansi"
#    export ENHANCD_DOT_SHOW_FULLPATH=1
#fi


#zplug "mollifier/anyframe"

zplug "mafredri/zsh-async"


zplug "b4b4r07/zplug-cd", lazy:yes
zplug "b4b4r07/zplug-rm", lazy:yes
zplug "b4b4r07/zplug-doctor", lazy:yes

zplug "modules/environment", from:prezto
zplug "modules/history", from:prezto
zplug "modules/directory", from:prezto

zplug "rupa/z", as:plugin, use:z.sh

#zplug "b4b4r07/zsh-history", as:command, use:init.zsh, rename-to:ff, hook-build:"make && sudo make install"
#if zplug check 'b4b4r07/zsh-history'; then
#    export ZSH_HISTORY_FILE="$HOME/.zsh_history.db"
#    ZSH_HISTORY_KEYBIND_GET_BY_DIR="^r"
#    ZSH_HISTORY_KEYBIND_GET_ALL="^r^a"
#    ZSH_HISTORY_KEYBIND_SCREEN="^r^r"
#    ZSH_HISTORY_KEYBIND_ARROW_UP="^p"
#    ZSH_HISTORY_KEYBIND_ARROW_DOWN="^n"
#fi

#zplug "modules/environment", from:prezto
#zplug "modules/history", from:prezto
#zplug "modules/directory", from:prezto
#zplug "modules/prompt", from:prezto
#zstyle ':prezto:*:*' color 'yes'
#zstyle ':prezto:module:editor' key-bindings 'emacs'
#zstyle ':prezto:module:prompt' theme 'pure'
#zstyle ':prezto:module:terminal' auto-title 'yes'

# Use the package as a command
# And accept glob patterns (e.g., brace, wildcard, ...)
#zplug "Jxck/dotfiles", as:command, use:"bin/{histuniq,color}"

# Can manage everything e.g., other person's zshrc
#zplug "tcnksm/docker-alias", use:zshrc

# Disable updates using the "frozen" tag
#zplug "k4rthik/git-cal", as:command, frozen:1

# Grab binaries from GitHub Releases
# and rename with the "rename-to:" tag
#zplug "junegunn/fzf-bin", \
#    from:gh-r, \
#    as:command, \
#    rename-to:fzf, \
#    use:"*darwin*amd64*"

#zplug 'junegunn/fzf', { 'do': { -> fzf#install() } }
#zplug "junegunn/fzf-bin", \
#    from:gh-r, \
#    as:command, \
#    rename-to:fzf, \
#    use:"*darwin*amd64*"
#zplug "junegunn/fzf", use:"shell/*.zsh", defer:2
# Supports oh-my-zsh plugins and the like



# Load if "if" tag returns true
#zplug "lib/clipboard", from:oh-my-zsh, if:"[[ $OSTYPE == *darwin* ]]"

# Run a command after a plugin is installed/updated
# Provided, it requires to set the variable like the following:
# ZPLUG_SUDO_PASSWORD="********"

# Can manage gist file just like other packages
#zplug "b4b4r07/79ee61f7c140c63d2786", \
#    from:gist, \
#    as:command, \
#    use:get_last_pane_path.sh

# Support bitbucket
#zplug "b4b4r07/hello_bitbucket", \
#    from:bitbucket, \
#    as:command, \
#    use:"*.sh"

# Rename a command with the string captured with `use` tag
#zplug "b4b4r07/httpstat", \
#    as:command, \
#    use:'(*).sh', \
#    rename-to:'$1'

# Group dependencies
# Load "emoji-cli" if "jq" is installed in this example
# zplug "stedolan/jq", \
#     from:gh-r, \
#     as:command, \
#     rename-to:jq
# zplug "b4b4r07/emoji-cli", \
#     on:"stedolan/jq"
# Note: To specify the order in which packages should be loaded, use the defer
#       tag described in the next section

# Set the priority when loading
# e.g., zsh-syntax-highlighting must be loaded
# after executing compinit command and sourcing other plugins
# (If the defer tag is given 2 or above, run after compinit command)

# Can manage local plugins
#zplug "~/.zsh", from:local

zplug "zsh-users/zsh-syntax-highlighting", defer:3
zplug "zsh-users/zsh-history-substring-search", on:"zsh-users/zsh-syntax-highlighting"
zplug "zsh-users/zsh-autosuggestions", on:"zsh-users/zsh-history-substring-search"
zplug "modules/completion", from:prezto, on:"zsh-users/zsh-history-substring-search"

bindkey '^[[A' history-substring-search-up
bindkey '^[[B' history-substring-search-down

zplug "sindresorhus/pure", use:"pure.zsh", as:theme, defer:3

# Install plugins if there are plugins that have not been installed
if ! zplug check; then
  echo; zplug install
fi

# Then, source plugins and add commands to $PATH
zplug load

# Aliases
alias ls='ls -aG'
alias ll='ls -l'

alias 'cd..'='cd ..'

alias g='git'
alias gst='git status'
alias ga='git add'
alias gb='git branch'
alias gbr='git branch -r'
alias gbdel='git branch -D'
alias gbdelr='git push origin --delete'
alias gc='git commit'
alias gco='git checkout'
alias gd='git diff'
alias gf='git fetch'
alias gitcurrbranch='git rev-parse --abbrev-ref HEAD'
alias gpthis='git push -u origin `gitcurrbranch`'
alias gum='git branch --no-merged `gitcurrbranch`'
alias gumr='git branch -r --no-merged `gitcurrbranch`'
alias grh='git reset HEAD'

alias di='docker images'
alias ds='docker stop'
alias dsa='docker stop (docker ps -a -q)'
alias drm='docker rm'
alias drma='docker rm (docker ps -a -q)'
alias drmia='docker rmi (docker images -a -q -f dangling=true)'

alias dc='docker-compose'
alias dm='docker-machine'

alias whs='wormhole send'
alias whr='wormhole receive'

HISTFILE=$HOME/.zsh-history
HISTSIZE=100000
SAVEHIST=50000
HISTFILESIZE=$HISTSIZE
ZSH_AUTOSUGGEST_USE_ASYNC=true
setopt inc_append_history
setopt share_history

[ -s "/usr/local/bin/rbenv" ] && eval "$(rbenv init -)"
[ -s "/usr/local/bin/direnv" ] && eval "$(direnv hook zsh)"

export PATH="$HOME/node_modules/.bin:$PATH"

# NVM
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

# place this after nvm initialization!
autoload -U add-zsh-hook
load-nvmrc() {
  local node_version="$(nvm version)"
  local nvmrc_path="$(nvm_find_nvmrc)"

  if [ -n "$nvmrc_path" ]; then
    local nvmrc_node_version=$(nvm version "$(cat "${nvmrc_path}")")

    if [ "$nvmrc_node_version" = "N/A" ]; then
      nvm install
    elif [ "$nvmrc_node_version" != "$node_version" ]; then
      nvm use
    fi
  elif [ "$node_version" != "$(nvm version default)" ]; then
    echo "Reverting to nvm default version"
    nvm use default
  fi
}
#add-zsh-hook chpwd load-nvmrc
#load-nvmrc

#export PERL5LIB="$HOME/.cpan/build:$PERL5LIB"
export PERLBREW_ROOT="$HOME/.perl5"
[ -s "${PERLBREW_ROOT}" ] && source ${PERLBREW_ROOT}/etc/bashrc
export PATH="$HOME/bin:$PATH"

ctrlp() {
  </dev/tty vim -c CtrlP
}
zle -N ctrlp

