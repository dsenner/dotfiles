" Map leader to ,
let mapleader=","
let g:mapleader = ","

set nocompatible              " be iMproved, required
set encoding=utf8

filetype plugin indent on

" Disable the bell
"set vb


" Load plugins
if filereadable(expand("$HOME/.vim/plugs.vimrc"))
  source $HOME/.vim/plugs.vimrc
endif

"let base16colorspace=256  " Access colors present in 256 colorspace

syntax enable
set background=dark
"colorscheme solarized
"colorscheme base16-railscasts

" Other settings
set mouse=a

set clipboard=unnamed
set pastetoggle=<Leader>v
set history=10000

" Splitting views
set splitbelow
set splitright

"Swap & Tempfiles
set nobackup
set nowritebackup
set noswapfile

" Fix indentation
"set expandtab
set tabstop=2
set shiftwidth=2
"set softtabstop=2
set autoindent
set smartindent
set nosmarttab
set backspace=indent,eol,start

" Better search matching
set ignorecase
set smartcase
set hlsearch

" Show a line where the cursor is
" NOTE don't enable. Will make scrolling slow as fuck..!
" set cursorline

" Show line numbers
set number

" Show matching bracket
set showmatch

" Show what mode we have
set showcmd

" Enable syntax highlighting
syntax enable

" Remove highlight after search
nmap <silent> ,/ :nohlsearch<CR>

" Tag cycles through panes/windows
map <tab> <C-W>W
map <s-tab> <C-W>w

" Tab to go next buffer
" Shift-Tab to go previous buffer
"nnoremap <Tab> :bn<CR>
"nnoremap <S-Tab> :bp<CR>

" Repeat commands over range
xnoremap . :norm.<CR>

xnoremap @ :<C-u>call ExecuteMacroOverVisualRange()<CR>

function! ExecuteMacroOverVisualRange()
  echo "@".getcmdline()
  execute ":'<,'>normal @".nr2char(getchar())
endfunction

set wildmenu
set wildmode=longest,list

" w!! will write the file as sudo
cnoremap w!! %!sudo cat - > %

" When editing a file, always jump to the last known cursor position.
" Don't do it when the position is invalid or when inside an event handler
" (happens when dropping a file on gvim).
augroup rememberfile
	autocmd!
	autocmd BufReadPost *
				\ if line("'\"") > 0 && line("'\"") <= line("$") |
				\   exe "normal g`\"" |
				\ endif |

	" Save the view when unloading the buffer.
	autocmd BufUnload ? silent mkview

	" Automagically restore the view when reading the buffer.
	autocmd BufRead * silent loadview
augroup END

" Moar undo
set undofile
set undodir=$HOME/.vim/_temp
set undolevels=1000
set undodir=$HOME/.vim/undo

let g:SaveUndoLevels = &undolevels
let g:BufSizeThreshold = 1000000

augroup undoSettings
  autocmd!
  " Store preferred undo levels
  au VimEnter * let g:SaveUndoLevels = &undolevels
  " Don't use a swap file for big files
  au BufReadPre * if getfsize(expand("<afile>")) >= g:BufSizeThreshold | setlocal noswapfile | endif
  " Upon entering a buffer, set or restore the number of undo levels
  au BufEnter * if getfsize(expand("<afile>")) < g:BufSizeThreshold | let &undolevels=g:SaveUndoLevels | hi Cursor term=reverse ctermbg=black guibg=black | else | set undolevels=-1 | hi Cursor term=underline ctermbg=red guibg=red | endif
augroup END

augroup fileTypes
  " All *.md files should be detected as markdown!
  autocmd BufNewFile,BufRead *.md set filetype=markdown
  autocmd BufNewFile,BufRead Gemfile set filetype=ruby
  autocmd BufNewFile,BufRead Vagrantfile set filetype=ruby
  autocmd BufNewFile,BufRead Berksfile set filetype=ruby
augroup END

" Remove trailing whitespace
" Handled by EditorConfig
"autocmd BufWritePre * :%s/\s\+$//e

" Indent and outdent keep visual selection
"vnoremap > >gv
"vnoremap < <gv

nnoremap <A-Down> :m .+1<CR>==
nnoremap <A-Up> :m .-2<CR>==
inoremap <A-Down> <Esc>:m .+1<CR>==gi
inoremap <A-Up> <Esc>:m .-2<CR>==gi
vnoremap <A-Down> :m '>+1<CR>gv=gv
vnoremap <A-Up> :m '<-2<CR>gv=gv

