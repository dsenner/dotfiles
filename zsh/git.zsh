alias g='git'
alias gst='git status -s'
alias ga='git add'
alias gb='git branch'
alias gc='git commit'
alias gco='git checkout'
alias gd='git diff'
alias gp='git push'
alias gbranch='git rev-parse --abbrev-ref HEAD'
alias gpthis='git push -u origin $(gbranch)'
alias gl='git log --oneline --decorate'
