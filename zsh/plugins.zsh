source $ZSH_FILES/plugins/z.sh
source $ZSH_FILES/plugins/colored-man-pages.zsh
source $ZSH_FILES/plugins/zsh-completions/zsh-completions.plugin.zsh
source $ZSH_FILES/plugins/zsh-autosuggestions/zsh-autosuggestions.plugin.zsh
source $ZSH_FILES/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.plugin.zsh
source $ZSH_FILES/plugins/zsh-history-substring-search/zsh-history-substring-search.zsh

#source ~/src/zaw/zaw.zsh
